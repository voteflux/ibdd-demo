#!/usr/bin/env python3

import binascii
import os
import random

from ibdd.bot.groups import groups
from ibdd.utils import *

issue_weights = {}
for _ in range(3):
    issue_weights[random.choice(list(groups.keys()))] = random.choice(["↑", "↓"]) * random.randint(1,3)
print(issue_weights)


api('make_issue', {"name": binascii.hexlify(os.urandom(4)).decode(), 'supporting': json.dumps(issue_weights, ensure_ascii=False)})

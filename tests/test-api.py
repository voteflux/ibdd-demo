import binascii
import os
import random
import unittest

from ibdd.bot.groups import groups
from ibdd.utils import *

users = {}


def notify_of_random_user():
    user = {
        'password': binascii.hexlify(os.urandom(16)).decode()
    }
    resp = api('notify_of_user', user)
    assert resp.status_code == 200
    print(resp)
    print(resp.json())
    users[resp.json()['id']] = user['password']


def create_random_issue():
    issue_weights = {}
    for _ in range(3):
        issue_weights[random.choice(list(groups.keys()))] = random.choice(["↑", "↓"]) * random.randint(1, 3)
    print(issue_weights)

    ans = api('debug_make_issue', {"name": binascii.hexlify(os.urandom(4)).decode(), 'supporting': json.dumps(issue_weights, ensure_ascii=False)})
    print('Create issue response:', ans)
    assert ans.status_code == 200


def create_random_bids():
    for uid, pw in users.items():
        pass


class MyTest(unittest.TestCase):

    def testAll(self):
        run_many(notify_of_random_user, 4)
        run_many(create_random_issue, 5)
        pretty_dumps(api('all_issues').json())


if __name__ == "__main__":
    unittest.main()
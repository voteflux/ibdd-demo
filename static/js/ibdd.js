const IbddApp = angular.module('IbddApp', ['ngMaterial', 'ngStorage', 'ngAnimate', 'timer']);

IbddApp.config(function($locationProvider){
    $locationProvider.html5Mode(true);
});

IbddApp.controller('IbddController', function($scope, $http, $location, $timeout, $mdSidenav, $mdDialog, $mdMedia, $localStorage, $log){
    $scope.$mdMedia = $mdMedia;

    const ibdd = this;

    ibdd.api = (route, data, successCb) => {
        const isLocal = ['localhost', '127.0.0.1'].indexOf($location.host()) != -1
        const url = (isLocal ? 'http://localhost:5000/' : 'https://ibdd-dev-backend.voteflux.org/') + 'api0/' + route;
        $log.log("calling", url, data)
        if (data === undefined || data === null) {
            $http.get(url).then(successCb, handleError)
        } else {
            $http.post(url, data).then(successCb, handleError)
        }
    }


    ibdd.currentNavPage = '';  // default set elsewhere
    ibdd.credits = 0;
    ibdd.makeIssue = (title, supporting, votingOpen) => {return {title: title, supporting: supporting, votingOpen: votingOpen, visible: true} }
    ibdd.getIssues = () => {
        ibdd.api('all_issues', null, (data) => {
            issues = data.data.issues
            _.map(issues, (i) => {
                i.visible = true;
                $log.log(i.auction_close)
                _.map(['issue_time', 'auction_close', 'voting_close'], (d) => { i[d] = new Date(i[d]); })
                $log.log((i.auction_close - Date.now()) / 1000 / 60 / 60)
                nowish = new Date();
                i.votingOpen = i.auction_close < nowish && nowish < i.voting_close;
                i.title = i.issue_name;
            })
            ibdd.issues = issues;
        })
    }
    ibdd.pageLoad = Date.now()
    ibdd.filteredIssues = (voting) => _.filter(ibdd.issues, (e) => e.votingOpen == voting && e.visible)
    ibdd.getBalances = () => {
        ibdd.api('all_credit_balances', null, (data) => {
            ibdd.creditBalances = data.data;
            $log.log(data);
        })
    }
    ibdd.getVoteBalances = () => {
        ibdd.api('all_vote_balances', null, (data) => {
            ibdd.voteBalances = data.data;
        })
    }

    /* Nav Functions */
    ibdd.navGoTo = (navPage) => {
        ibdd.currentNavPage = navPage;
        $location.hash(navPage);
    }
    ibdd.navIs = (navPage) => navPage == ibdd.currentNavPage;
    ibdd.getNav = () => ibdd.currentNavPage;
    ibdd.loadNav = () => ibdd.navGoTo($location.hash() || 'intro');


    ibdd.genUserId = (password) => sha256(password).slice(0,16);
    ibdd.initUser = (password) => {
        $localStorage.password = password;
        ibdd.password = password;
        ibdd.userid = ibdd.genUserId(password);
    }
    ibdd.showGetPassword = (event) => {
        var pwPrompt = $mdDialog.prompt()
            .title('Please enter a pass-code.')
            .textContent('This should be a unique, _secret_, code or phrase which will be used to identify you. Since this is just a test, feel free to just use your name in lowercase.\n\nDO NOT USE YOUR REGULAR PASSWORD')
            .placeholder('some password I don\'t use for anything else...')
            .ok('Okay');
        if (event) pwPrompt.targetEvent(event)
        $mdDialog.show(pwPrompt).then(ibdd.initUser)
    }
    ibdd.loadPassword = () => {
        if ($localStorage.password) ibdd.initUser($localStorage.password);
        else ibdd.showGetPassword();
    }


    const issueDialogFactory = (templateUrl) => (issue, ev) => {
        $mdDialog.show({
            templateUrl: templateUrl,
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            locals: {
                ibdd: ibdd,
                issue: issue
            },
            controller: DialogController
        }).then();
        function DialogController($scope, $mdDialog, issue, ibdd){
            $scope.issue = issue;
            $scope.ibdd = ibdd;
            $scope.close = function(){
                $mdDialog.hide();
            }
        }
    }
    /* Voting and Auction Functions */
    ibdd.votes = {};
    ibdd.votes.bid = issueDialogFactory('ibdd-bid-dialog.html');
    ibdd.votes.issueInfo = issueDialogFactory('ibdd-issue-info-dialog.html');
    ibdd.votes.keep = issueDialogFactory('ibdd-issue-keep-confirmation-dialog.html');
    ibdd.votes.voteFor = issueDialogFactory('ibdd-vote-for-dialog.html');
    ibdd.votes.voteAgainst = issueDialogFactory('ibdd-vote-against-dialog.html');
    ibdd.votes.help = issueDialogFactory('ibdd-help-dialog.html');
    ibdd.votes.dismiss = (issue, ev) => { issue.visible = false; }


    /* Admin Functions */
    ibdd.newIssue = {};
    ibdd.newIssue.create = () => {
        ibdd.api('make_issue', {password: ibdd.password, name: ibdd.newIssue.name, supporting: ibdd.newIssue.supporting}, (data) => {
            ibdd.newIssue.name = ibdd.newIssue.supporting = "";
            ibdd.getIssues();
        })
    }


    /* Side Menu Functions */
    ibdd.toggleMenuSidenav = buildDelayedToggler('menuSidenav');



    // Taken from angular tutorial -- currently lags by 200 ms; not sure if we want that //shrug

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
      return debounce(function() {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }, 100);
    }

    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait, context) {
      var timer;

      return function debounced() {
        var context = $scope,
            args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function() {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    }





    /*
     * Startup Functions
     */
    onStartup = () => {
        ibdd.loadPassword();
        ibdd.loadNav();
        ibdd.getIssues();
        ibdd.getBalances();
        ibdd.getVoteBalances();
    }
    setTimeout(onStartup, 200);
});

IbddApp.directive('ibddUserSettings', () => {return { restrict: 'E', templateUrl: 'ibdd-user-settings.html' }})
       .directive('ibddIntroduction', () => {return { restrict: 'E', templateUrl: 'ibdd-introduction.html' }})
       .directive('ibddAdmin', () => {return { restrict: 'E', templateUrl: 'ibdd-admin.html' }})
       .directive('ibddBalances', () => {return { restrict: 'E', templateUrl: 'ibdd-balances.html' }})
       .directive('ibddIssues', () => {return { restrict: 'E', templateUrl: 'ibdd-issues.html' }})
       .directive('ibddIssuesColumn', () => {
            return {
                restrict: 'E',
                scope: { ibddEven: '=', ibdd: '=', ibddVoting: '=' },
                templateUrl: 'ibdd-issues-column.html'
            }
       })

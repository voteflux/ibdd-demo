import logging
logging.basicConfig(level=logging.INFO)

import asyncio
import time
import random
import binascii
import json
import os

import ibdd.db as db
from ibdd.bot.groups import groups


async def manage_scheduled_tasks():
    asyncio.ensure_future(create_new_issues_forever())


async def _generic_repeating_task(f, period: int, offset=0):
    assert asyncio.iscoroutinefunction(f)
    assert period >= 10
    logging.info("Scheduled %s, with period %d and offset %d" % (f, period, offset))
    while True:
        await asyncio.sleep(period - (int(time.time() - offset) % period))
        logging.info("Running %s, with period %d" % (f, period))
        await f()
        logging.info("Completed %s, with period %d" % (f, period))
        await asyncio.sleep(2)  # sleep to ensure we don't trigger this twice in one period


async def create_new_issues():
    logging.info("Creating new issues")
    issue_weights = {}
    for _ in range(3):
        issue_weights[random.choice(list(groups.keys()))] = random.choice(["↑", "↓"]) * random.randint(1, 5)
    print(issue_weights)
    issue = {"name": binascii.hexlify(os.urandom(4)).decode(),
             'supporting': json.dumps(issue_weights, ensure_ascii=False),
             'userid': "SYSTEM"}
    logging.info("Created issue: %s" % db.create_issue(**issue).to_json())

async def create_new_issues_forever():
    period = 15        # 1 min
    period = 60*60     # 1 hr
    period = 60*60*24  # 24 hrs
    offset = 60*60*10  # 10 hrs
    await _generic_repeating_task(create_new_issues, period, offset)  # once per day with 10hr offest => should be midnight


if __name__ == "__main__":
    asyncio.ensure_future(manage_scheduled_tasks())
    asyncio.get_event_loop().run_forever()

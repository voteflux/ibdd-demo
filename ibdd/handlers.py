from collections import defaultdict
import binascii
import os
import random
import json

from tornado.web import RequestHandler, HTTPError


import ibdd.db as db
from ibdd.bot.groups import groups
from .utils import *


def extract_data(f):
    def inner(self, *args, **kwargs):
        return f(self, get_json_body(self), *args, **kwargs)
    return inner


def authenticate(f):
    def inner(self, *args, **kwargs):
        data = get_json_body(self)
        if 'password' not in data:
            self.send_error(403)
            return
        data['userid'] = sha256hex(data['password'])
        return f(self, data, *args, **kwargs)
    return inner


def auth_admin(f):
    def inner(self, *args, **kwargs):
        data = get_json_body(self)
        if 'password' not in data:
            self.send_error(403)
            return
        data['userid'] = sha256hex(data['password'])
        if db.get_admin_id() != data['userid']:
            self.send_error(403)
            return
        return f(self, data, *args, **kwargs)
    return inner


class BaseHandler(RequestHandler):
    def set_default_headers(self, *args, **kwargs):
        self.set_header(r'Access-Control-Allow-Origin', r'*')
        self.set_header(r'Access-Control-Allow-Headers', r'Content-Type')

    def options(self, *args, **kwargs):
        pass


class InfoHandler(BaseHandler):
    def get(self, *args, **kwargs):
        self.write({
            'status': 'running'
        })


class MakeIssueHandler(BaseHandler):
    @auth_admin
    def post(self, data):
        issue = db.create_issue(**data)
        self.write(issue.to_json())


class IssuesHandler(BaseHandler):
    def get(self):
        issues = db.all_issues()
        self.write({'issues': [issue.to_json() for issue in issues]})


class NotifyOfUserHandler(BaseHandler):
    @authenticate
    def post(self, data):
        user = db.notify_of_user(**data)
        self.write(user.to_json())


class UserHandler(BaseHandler):
    def get(self):
        users = db.all_users()
        self.write({'users': [user.to_json() for user in users]})


class CreditBalancesHandler(BaseHandler):
    def get(self):
        all_balances = db.all_balances()
        d_balances = {}
        for balance in all_balances:
            d_balances[balance.user_id] = balance.balance
        self.write(d_balances)


class VoteBalancesHandler(BaseHandler):
    def get(self):
        all_vote_balances = db.all_vote_balances()
        d_v_balances = defaultdict(dict)
        for v_b in all_vote_balances:
            d_v_balances[v_b.user_id][v_b.issue_id] = v_b.balance
        print(d_v_balances)
        self.write(d_v_balances)


class CastVotesHandler(BaseHandler):
    @authenticate
    def post(self, data):
        db.log_user_vote(**data)


class DebugMakeIssueHandler(BaseHandler):
    def get(self):
        issue_weights = {}
        for _ in range(3):
            issue_weights[random.choice(list(groups.keys()))] = random.choice(["↑", "↓"]) * random.randint(1, 3)
        print(issue_weights)
        issue = {"name": binascii.hexlify(os.urandom(4)).decode(), 'supporting': json.dumps(issue_weights, ensure_ascii=False)}
        self.write(db.create_issue(**issue).to_json())


# class







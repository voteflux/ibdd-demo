inverted_groups = {
    'pensioners': 'P',
    'youth': 'Y',
    'business': 'B',
    'home-owners': 'H',
    'greenies': 'G',
    'christians': 'C',
    'lgbtqi': 'L',
    'tradies': 'T',
}

groups = dict({v: k for k, v in inverted_groups.items()})
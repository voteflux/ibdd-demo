from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.engine import create_engine

from .models import *
from .config import config


engine = create_engine(config.DATABASE_URL, echo=config.debug)
db = scoped_session(sessionmaker())
db.configure(bind=engine)


Base.metadata.create_all(engine)


def get_admin_id():
    return "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918"  # sha256("admin")
    return db.query(Users).filter(Users.is_admin is True).first().id


def create_issue(name, supporting, userid, **kwargs):
    new_issue = Issues(name, supporting, userid)
    _all_users = all_users()
    db.add(new_issue)
    db.commit()
    for user in _all_users:
        assert type(user) is Users
        db.add(VoteBalances(user.id, new_issue._id))
    db.commit()
    return new_issue


def all_issues():
    return db.query(Issues).all()


def notify_of_user(userid, **kwargs):
    if get_user(userid) is None:
        new_user = Users(userid)
        db.add(new_user)
        db.add(CreditBalances(userid, 100000))
        return new_user
    return get_user(userid)


def get_user(userid):
    return db.query(Users).filter(Users.id == userid).first()


def all_users():
    return db.query(Users).all()


def all_balances():
    return db.query(CreditBalances).all()


def all_vote_balances():
    return db.query(VoteBalances).all()


def log_user_vote(userid, issueid, inFavor: bool):
    db.query(VoteBalances).filter(VoteBalances.issue_id == issueid).filter(VoteBalances.user_id == userid)


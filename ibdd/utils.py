import asyncio
import logging
import json
import traceback
import re
import hashlib
import datetime
import requests


from .config import config


all_states = ['act', 'nsw', 'tas', 'vic', 'wa', 'sa', 'nt', 'qld']


async def rie(f, *args, **kwargs):
    return await loop().run_in_executor(None, f, *args, **kwargs)


def t_24():
    return datetime.datetime.now() + datetime.timedelta(hours=24)


def t_48():
    return datetime.datetime.now() + datetime.timedelta(hours=48)


def loop():
    return asyncio.get_event_loop()

async def wait_for_futures(fs):
    tasks = list(map(asyncio.ensure_future, fs))
    loop().run_until_complete(asyncio.wait(tasks))


def get_json_body(handler):
    body = handler.request.body
    body = body.decode() if type(body) is bytes else body
    return json.loads(body)


def pass_error(f):
    assert asyncio.iscoroutinefunction(f)
    async def inner(self, *args, **kwargs):
        try:
            return await f(self, *args, **kwargs)
        except Exception as e:
            logging.error("Exception Caught:")
            logging.error(e.args)
            logging.error(traceback.print_tb(e.__traceback__))
            self.set_status(500)
            self.finish({'error_args': list(map(str, e.args)), 'error_type': str(type(e))})
    return inner


def extract_postcode_from_doc(addr_doc):
    addr_full = addr_doc['address']
    pc = re.search(r'[0-9]{4}', addr_full[::-1])
    return None if pc is None else pc.group(0)[::-1]


def lookup_state(pc):
    if pc is None: pc = 'xxxx'
    lookup = {
        '0': 'nt',
        '3': 'vic',
        '5': 'sa',
        '4': 'qld',
        '6': 'wa',
        '7': 'tas',
    }
    if pc[0] in lookup:
        return lookup[pc[0]]
    lmsr = lambda s, e: list(map(str, range(s, e)))
    act_codes = lmsr(2600, 2618) + lmsr(2900, 2907) + lmsr(2911, 2915)
    if pc in act_codes:
        return 'act'
    if pc[0] == '2':
        return 'nsw'
    return 'unknown'


def get_state_regex(state):
    lookup = {
        'act': '(260[0-9])|(261[0124567])|(290[0-6])|(291[1-4])',
        'nsw': '2[0-9]{3}',
        'nt': '0[0-9]{3}',
        'vic': '3[0-9]{3}',
        'sa': '5[0-9]{3}',
        'qld': '4[0-9]{3}',
        'wa': '6[0-9]{3}',
        'tas': '7[0-9]{3}',
    }
    if state in lookup:
        return lookup[state]
    return None


state_from_doc = lambda d: lookup_state(extract_postcode_from_doc(d))


def gen_subscriber_hash(email):
    return hashlib.md5(email.lower().encode()).digest().hex()


def sha256hex(data_in):
    return hashlib.sha256(data_in.encode()).digest().hex()


def api(route, data=None):
    if route[0] == '/':
        raise ValueError("Include leading / you must not: " + route)
    url = "http://localhost:5000/api0/" + route
    if data is None:
        return requests.get(url)
    else:
        if 'password' not in data:
            data['password'] = 'admin'
        return requests.post(url, json=data)


def pretty_dumps(d):
    print(json.dumps(d, indent=4))


def run_many(f, n):
    for _ in range(n):
        f()

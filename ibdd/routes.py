from tornado.web import RedirectHandler, StaticFileHandler

from .handlers import *

routes = [
    (r"/", RedirectHandler, {"url": "/static/html/index.html"}),
    (r"/static/(.*)", StaticFileHandler, {'path': 'static'}),
    (r"/api0/info", InfoHandler),
    # (r"/api0/set_admin", SetAdminHandler),
    (r"/api0/make_issue", MakeIssueHandler),
    (r"/api0/all_issues", IssuesHandler),
    (r"/api0/notify_of_user", NotifyOfUserHandler),
    (r"/api0/all_users", UserHandler),
    (r"/api0/all_credit_balances", CreditBalancesHandler),
    (r"/api0/all_vote_balances", VoteBalancesHandler),
    (r"/api0/cast_votes", CastVotesHandler),
    # (r"/api0/make_bid", MakeBidHandler),
    # (r"/api0/all_bids", AllBidsHandler),
    # (r"/api0/claim_vote", ClaimVoteHandler),
    (r"/api0/debug_make_issue", DebugMakeIssueHandler)
]
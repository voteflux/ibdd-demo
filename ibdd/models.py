from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    ForeignKey,
    DateTime,
    Sequence,
    Float
)
import datetime


from .utils import *


Base = declarative_base()


class IbddMixin:
    def to_json(self):
        def process_attr(attr):
            if type(attr) is datetime.datetime:
                return attr.isoformat()
            return attr
        return dict({c.name: process_attr(getattr(self, c.name)) for c in self.__table__.columns})


class Instruction(IbddMixin, Base):
    __tablename__ = "instructions"
    _id = Column(Integer, Sequence('instruction_seq'), primary_key=True)
    serialized_instruction = Column(String())


class Votes(IbddMixin, Base):
    __tablename__ = "votes"
    id = Column(Integer, Sequence('votes_seq'), primary_key=True)
    issue_id = Column(Integer)
    volume = Column(Integer)
    favour_flag = Column(Boolean)


class Issues(IbddMixin, Base):
    __tablename__ = "issues"
    _id = Column(Integer, Sequence('issues_seq'), primary_key=True)
    issue_name = Column(String())
    supporting = Column(String())
    created_by = Column(String(), ForeignKey('users.id'))
    votes_for = Column(Integer, default=0)
    votes_against = Column(Integer, default=0)
    issue_time = Column(DateTime, default=datetime.datetime.now)
    auction_close = Column(DateTime, default=t_24)
    voting_close = Column(DateTime, default=t_48)

    def __init__(self, name, sup, user):
        self.issue_name = name
        self.supporting = sup
        self.created_by = user


class Users(IbddMixin, Base):
    __tablename__ = "users"
    _id = Column(Integer, Sequence('user_seq'), primary_key=True)
    id = Column(String(64), unique=True)  # hex sha256(pw)
    first_seen = Column(DateTime, default=datetime.datetime.now)

    def __init__(self, userid):
        assert len(userid) == 64
        self.id = userid


class VoteBalances(IbddMixin, Base):
    __tablename__ = "vote_balances"
    _id = Column(Integer, Sequence('vote_balance_seq'), primary_key=True)
    user_id = Column(String(64), ForeignKey('users.id'))
    issue_id = Column(Integer, ForeignKey('issues._id'))
    balance = Column(Integer, default=1)

    def __init__(self, userid, issueid, balance=1):
        self.user_id = userid
        self.issue_id = issueid
        self.balance = balance


class CreditBalances(IbddMixin, Base):
    __tablename__ = "credit_balances"
    _id = Column(Integer, Sequence('credit_balance_seq'), primary_key=True)
    user_id = Column(String, ForeignKey('users.id'), unique=True)
    balance = Column(Integer)

    def __init__(self, userid, balance):
        self.user_id = userid
        self.balance = balance




import logging
logging.getLogger().setLevel(logging.INFO)
logging.info("Set logging info.")


from tornado.web import Application
import tornado.ioloop

from ibdd.routes import routes
from ibdd.config import config

def make_app():
    return Application(routes, debug=True)


if __name__ == "__main__":
    app = make_app()
    app.listen(config.PORT)
    logging.info('Starting server on port %s' % config.PORT)
    tornado.ioloop.IOLoop.current().start()
    logging.info('Shutting Down Server')
